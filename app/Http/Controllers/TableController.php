<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class TableController extends Controller
{
    //
	public function tableProduct() {
		$products = Product::orderby('id')->get();
		return view('TableProducts')->with(['products'=>$products]);
	}
}
