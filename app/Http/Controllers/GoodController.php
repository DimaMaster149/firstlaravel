<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\Product;

class GoodController extends Controller
{

	public function product($id=null) {
		if($id==null) {
			$min_id = min(Product::select('shop_id')->get()->toArray());
			$products = Product::orderby('id')
				->where('shop_id', '=', $min_id['shop_id'])
				->get();
		} else {
			$products = Product::orderby('id')->where('shop_id', '=', $id)
				->get();
		}
		return response()->json($products);
	}

    public function shop()
    {
       $shops = Shop::orderby('id')->get();
       return response()->json($shops);
    }

	public function viewProduct($id) {
		$products = Product::orderby('id')->where('shop_id', '=', $id)
			->get();
		return response()->json($products);
	}

	public function destroy($id){
		try{
			Product::destroy($id);
			return response([], 204);
		} catch(\Exception $e){
			return response(['Deleting error', 500]);
		}
	}

}
