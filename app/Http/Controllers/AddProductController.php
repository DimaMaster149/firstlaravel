<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\Product;
class AddProductController extends Controller
{
	public function store(Request $request){
		$product=new Product ($request->all());
		$product->save();
		return view('welcome');
	}

	public function add(){
		return view('addProduct');
	}

	public function show($id){
		return response()->json(Product::find($id));
	}

	public function update(Request $request, $id){
		$product=Product::find($id);
		$product->update($request->all());
		return response()->json($product);
	}
}
