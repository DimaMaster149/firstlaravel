<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'shop_id', 'name', 'description', 'price', 'img'
    ];

    public function shop()
 {
   return $this->belongsTo(Shop::class);
 }
}
