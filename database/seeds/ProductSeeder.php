<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	    print_r("ps");
	    DB::table('products')->truncate();
	    factory(App\Product::class, 20)->create();
    }
}
