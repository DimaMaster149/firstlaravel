<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Product::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'description' => $faker->text(255),
	      'img' => $faker->image(public_path().'\images\products',200, 200, 'cats', false),
	      'shop_id' => $faker->numberBetween(1, 10),
        'price' =>$faker->randomFloat(2, 1, 100),
    ];
});
/*
$factory->define(App\Shop::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'address' => $faker->address(32),
    ];
});
*/
