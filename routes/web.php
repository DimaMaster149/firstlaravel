<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
/*Вывод продуктов*/
Route::get('/goods/products/{shop_id?}', 'GoodController@product')->name('product');
Route::get('/goods/shops', 'GoodController@shop')->name('shop');
Route::get('/table', 'TableController@tableProduct')->name('tableProduct');

/*Добавление продуктов*/
Route::get('/add', 'AddProductController@add');
Route::post('/add', 'AddProductController@store')->name('productStore');
/*Удаление продуктов*/
Route::delete('/goods/products/{id?}', 'GoodController@destroy')->name('productDelete');
/*Обновление продуктов*/
Route::get('products/{id?}', 'AddProductController@show');
Route::put('products/{id?}', 'AddProductController@update');

Route::get('/home', 'HomeController@index');
