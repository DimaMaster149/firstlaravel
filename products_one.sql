-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 23 2018 г., 19:18
-- Версия сервера: 5.6.37
-- Версия PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `products_one`
--

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2018_06_16_154611_create_products_table', 1),
(14, '2018_06_16_154715_create_shops_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `shop_id`, `name`, `description`, `price`, `img`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nobis', 'Vitae aut id fugiat at impedit dolor et. Dolorum est at rerum. Cumque libero est vitae qui ea at consequatur id.', 80, '08068a8144c74305040a52547cbf49bd.jpg', '2018-06-16 17:39:54', '2018-06-21 17:46:28'),
(2, 4, 'Error', 'Doloremque distinctio ratione cupiditate ea. Ipsam expedita voluptate commodi inventore qui sunt est. Dolorem rerum ut possimus quia maiores.', 62, '62ac53d6ff0405ebcf07901d9b7313cf.jpg', '2018-06-16 17:39:54', '2018-06-21 03:24:00'),
(3, 9, 'et', 'Eos eos id facere aperiam. Omnis qui laborum illo dolorem rerum. Autem a minus ea molestias laboriosam sed. Consectetur nihil incidunt facilis molestiae et eius sit. Recusandae rerum similique ea. Culpa ad cum doloribus voluptates minima iure omnis.', 93, '9bf76746c2487cb81458a157a21dce45.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(4, 4, 'molestiae', 'Nam rerum sit non tempora. Qui sint est et magnam iure odio non. Corrupti cum accusantium aut facere. Qui ipsa odio deserunt magni explicabo nobis.', 43, '4b07c790c5bcabfb6ce64cc539060e5f.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(5, 7, 'magni', 'Dolore velit ut itaque. Soluta corrupti rem ex. Vel quam laudantium necessitatibus sit. Iusto quis sunt qui exercitationem voluptas debitis reiciendis est.', 56, 'ecc1f6aba78dc7b08a4fdc6f4ce91eab.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(6, 5, 'et', 'Dolor dolorem et impedit natus quibusdam aut. Dolorem deserunt a et aut quod modi. Nisi blanditiis libero laboriosam dolorem magnam dolor expedita. Esse corporis molestiae esse doloremque vel.', 22, '6f433abe30a0d33321720f71471db824.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(7, 9, 'nisi', 'Velit enim omnis minus dignissimos. Deserunt harum perspiciatis nihil delectus sit. Ut error mollitia explicabo mollitia enim et sequi.', 99, 'ba554267d409d29795f82218839da192.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(8, 2, 'aliquid', 'Autem nostrum magnam doloribus fugit voluptas nihil. Labore est sed dolorum quis ullam. Illo eius numquam et pariatur tenetur sed. Qui quo eos voluptates in. Doloremque animi dignissimos culpa atque. Natus est quia qui repellat.', 80, 'b3627a5eee097379cae3e1b591efc24b.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(9, 10, 'optio', 'Deserunt aut voluptatibus dolores aut blanditiis eos est ut. Aliquid omnis et iusto autem rem et dicta. Hic non delectus rerum. Et asperiores atque aspernatur.', 84, 'ba3f284bcdf77f3105eb5d3f3cd875dc.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(10, 5, 'dolor', 'Excepturi et nisi voluptatem molestiae dolor nesciunt. Animi ducimus omnis est ipsum iusto harum soluta asperiores. Consequuntur sint voluptatem unde culpa. Sed ut nisi cumque veritatis maiores numquam.', 6, 'ccf9a1e6fd6f93bc913558b48496e392.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(11, 6, 'quia', 'Aspernatur aliquid qui aut perferendis saepe dolorem. Ipsam atque et a at et in. Occaecati provident dolor ut neque. Possimus sed nemo assumenda ipsum natus.', 49, '4f5228f4e983def8521a43ba9be51618.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(12, 2, 'id', 'Est perspiciatis qui nihil consequatur cumque et laboriosam iusto. Officia est itaque illum qui. Ea aut dolor et rem laudantium.', 73, '381702df63e681bf69b24047c30d2bf9.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(13, 1, 'rem', 'Reprehenderit est numquam impedit quidem placeat cupiditate. Dolores ipsam sed qui reiciendis voluptate omnis. Facere eaque dolorem et rem nihil maxime. Ut quis fuga rem aliquid tenetur.', 51, '40aeadb143e257545d902d17da39915a.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(14, 4, 'saepe', 'Iusto ad est omnis facere culpa neque ex. Dolor quam corporis incidunt saepe quia ab quia velit. Voluptas repellendus est qui amet tempora facere iusto non.', 13, '2dcf980bfc7741a4cbbd7c61f534d6ea.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(15, 5, 'quia', 'Corporis provident sit accusantium hic ut. In voluptas dolorem amet sit maiores accusantium. In doloribus accusamus velit sunt. Animi provident quis molestiae veritatis voluptate.', 35, 'f2f87a470a9a68639d16d12de82e45e6.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(16, 2, 'ea', 'Id est consequatur eveniet. Temporibus vel voluptas dolor officiis eos illum nihil. Iure incidunt odio odio mollitia consequuntur inventore doloremque omnis.', 93, 'f0284b0df360aaff6aedbe04ff324aa1.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(17, 4, 'quaerat', 'Vero alias voluptas itaque sapiente corrupti aut. Qui asperiores corrupti et velit ut similique minus. Fugit porro amet dolor deserunt est quos.', 81, '79265c69fc120d2a9218fde91c20ac3d.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(18, 1, 'nulla', 'Id et ratione vel nam ut id. Et animi sapiente in doloremque labore a voluptas nesciunt. Et voluptas ut incidunt itaque cum.', 22, '8748457fb10bc13c0d924bc94c4fe107.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(19, 3, 'vel', 'Saepe repellat officia aut quia maiores. Dolorem voluptate expedita in est quae eligendi qui. Et quo quis est voluptas.', 94, '7f843fa738a9d3b1230fe3b84b16b6b2.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54'),
(20, 1, 'corporis', 'Libero reprehenderit dolores distinctio. Reiciendis nihil rerum pariatur omnis minus commodi.', 39, '0cbb5ca3f0a727fbc3953ef265bc2453.jpg', '2018-06-16 17:39:54', '2018-06-16 17:39:54');

-- --------------------------------------------------------

--
-- Структура таблицы `shops`
--

CREATE TABLE `shops` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `shops`
--

INSERT INTO `shops` (`id`, `name`, `address`, `created_at`, `updated_at`) VALUES
(1, 'laboriosam', '37295 Dayana Park\nReynoldshaven, NY 76660', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(2, 'delectus', '40494 Mariah Forges Apt. 718\nHicklestad, NC 35386', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(3, 'eaque', '35741 Ernestina Trace Apt. 714\nBinsborough, OR 51780', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(4, 'sit', '403 Brekke Mount\nFelipeside, NJ 74586', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(5, 'qui', '69142 Aufderhar Causeway Apt. 335\nNorth Delta, OH 09420-0526', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(6, 'officia', '475 Runolfsdottir Falls\nSouth Hazelborough, NM 54957-4052', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(7, 'in', '148 Terry Tunnel\nWest Lornashire, MI 72897', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(8, 'nihil', '295 Harvey Harbor\nEast Jacklynfurt, VA 46960-1443', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(9, 'voluptatem', '64650 Elenor Lodge\nLake Lacey, WI 80211-7488', '2018-06-16 17:39:06', '2018-06-16 17:39:06'),
(10, 'tempore', '6518 Cindy Crossroad Suite 058\nEmiliachester, MN 50159-0928', '2018-06-16 17:39:06', '2018-06-16 17:39:06');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_shop_id_index` (`shop_id`);

--
-- Индексы таблицы `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
