require('./bootstrap');
window.Vue = require('vue');
import Datatable from 'vue2-datatable-component'
Vue.use(Datatable);

Vue.use(require('vue-resource'));

import VueRouter from 'vue-router/dist/vue-router.js';
Vue.use(VueRouter);
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=_token]').attr('content');

const Example = Vue.component('example', require('./components/Example.vue'));
const Products = Vue.component('products', require('./components/Products.vue'));
const AddProduct = Vue.component('addProduct', require('./components/AddProduct.vue'));
const UpdateProduct = Vue.component('updateProduct', require('./components/UpdateProduct.vue'));
const Navbar = Vue.component('navbar', require('./components/Navbar.vue'));
const TableProducts = Vue.component('tableProducts', require('./components/TableProducts.vue'));

const router = new VueRouter({
    mode: 'history',
    routes: [
      { path:"/example", component: Example },
      { path:"/products", component: Products },
      { path:"/products/add", component: AddProduct },
      { path:"/products/:product/update", component: UpdateProduct},
      { path:"/products/table", component: TableProducts},
    ]
});

const app = new Vue({
    router: router,
}).$mount('#app');
