<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Example</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <style>
        form{
            margin-top: 50px;
        }
    </style>

</head>
<body>
<div class="container">
    <form method="POST" action="{{route('productStore')}}" enctype='multipart/form-data' >
        <div class="form-group">
            <label for="name"> Enter product name </label>
            <input type="text" class="form-control" name="name" value="">
        </div>
        <div class="form-group">
            <label for="description"> Enter product description </label>
            <textarea type="text" class="form-control" name="description" > </textarea>
        </div>
        <div class="form-group">
            <label for="shop_id"> Enter shop id </label>
            <input type="number" class="form-control" name="shop_id" value="" placeholder="From 1 to 10">
        </div>
        <div class="form-group">
            <label for="price"> Enter product price </label>
            <input type="number" class="form-control" name="price" value="" >
        </div>
        <div class="form-group">
            <label for="img"> Enter file </label>
            <input type="file" class="form-control" name="img" value="" placeholder="Choose product image">
        </div>

        <button type="submit" class="btn btn-success" name="Submit">Create product</button>
        {{csrf_field() }}
    </form>
</div>

</body>
</html>
